# Camelot Platform

[Camelot](https://gitlab.com/camelot-project/camelot) is open-source camera
trapping software for wildlife researchers and conservationists.

The Camelot Platform seeks to leverage Camelot in order to simplify the
creation of new tools and techniques which work with camera trap data.

## Table of Contents

* [Status](#status)
* [Motivation](#motivation)
* [Design principles](#design-principles)
* [Limitations](#limitations)
* [Future](#future)
* [License](#license)

## Status

The changes described below are significant and foundational and will take
several [minor releases](https://semver.org/) to land in full. The first parts
of which are expected to land in what would be Camelot 1.5.0.

## Motivation

At time of writing, Camelot is a relatively large codebase written in a
reasonably [esoteric programming language](http://clojure.org/), which makes
it difficult for the community contribute and extend. In fact, in the 2+ years
since the release of Camelot, there have not been any contributions to the
codebase, and the
[DSL](https://en.wikipedia.org/wiki/Domain-specific_language) for [reporting
add-ons](https://camelot-project.readthedocs.io/en/latest/administration.html#custom-reports)
has proven to have a
[high](https://groups.google.com/forum/#!topic/camelot-project/JzEMLvBA7nw)
[barrier](https://groups.google.com/forum/#!topic/camelot-project/JZwWmXieOWE)
[to](https://groups.google.com/forum/#!topic/camelot-project/iDqCShAW6CY)
[adoption](https://groups.google.com/forum/#!topic/camelot-project/stUmGv6VLRM).

This is the case despite Camelot being open-source, reasonably well-written
and well-documented.

As an application, Camelot has captured significant mindshare among
researchers and conservationists and offers [the best combination of
flexibility and functionality](https://doi.org/10.1111/aje.12550) among the
software for camera trap data management currently available.

Due to barriers around extending the current range of software, someone
looking to make an incremental improvement must either duplicate 98% of the
effort of its predecessors or grapple with extending a large, unfamiliar
codebase. While camera trap software was simple and not well-featured, it was
not unreasonable to write something from scratch. But the level of
sophistication is now such that this would be a very significant undertaking.

In other words, as software for camera trap data becomes more sophisticated,
there is increasing pressure for people to find solutions which extend what is
already available.

The Camelot Platform is an initiative to create pressure from the other end:
to make what is already available more readily extended.

## Design principles

The principles of the Camelot Platform are focused towards ensuring the
success of would-be add-on and integration authors. Specifically:

1. [authors can freely choose their own tech stacks](#authors-can-freely-choose-their-own-tech-stacks)
2. [simple to register an add-on](#simple-to-register-an-add-on)
3. [documentation is both accessible and comprehensive](#documentation-is-both-accessible-and-comprehensive)
4. [errors result in accurate and useful messages](#errors-result-in-accurate-and-useful-messages)

### Authors can freely choose their own tech stacks

Open source software is generally a necessary component for extensibility by a
global community, but it's not sufficient: the community must also be *able*
to build upon the codebase. As the available software grows in functionality,
it also grows in complexity, creating a high barrier to entry as a
contributor.

In the case of Camelot, things are further complicated by the fact that it is
written in Clojure. But despite the not using a mainstream language, Camelot
still needs to make itself extensible.

This does not mean that Camelot should be rewritten. There's little evidence
to suggest that being written in any one language -- mainstream or not -- will
rectify this. Take
[TRAPPER](https://bitbucket.org/trapper-project/trapper-project/) a
well-written, open source software in Python, that has seen only author
developing it over its 2+ year lifetime.

One of the most successful projects for attracting contributors is
[CamtrapR](https://cran.r-project.org/web/packages/camtrapR/index.html) which
at time of writing has 6 contributors in addition to its 3 authors. This is
written in R, which is well known in the field, but would not be a suitable
language for implementing something like Camelot.

Researchers and conservationists should not be expected to be technical
specialists and it is important that a move towards extensibility in this
field minimises the requirement to learn technologies such as programming
languages.

Therefore the Camelot Platform will allow for add-ons to be written with
whichever technology stack the author(s) are most comfortable.

### Simple to register an add-on

Integrating with Camelot should be a trivial compared to building the add-on
functionality itself.

Camelot will use a simple, JSON-based descriptor for add-ons to declare how
they should be handled within Camelot. See the [corresponding
section](#introduce-an-add-on-system) under the [implementation
strategy](#implementation-strategy) for details.

Camelot may need to be restarted to provide or change an add-on descriptor,
though this is not expected to really impede development of the add-on itself.

Furthermore, Camelot will provide *sensible defaults* for features where
possible to allow authors to build out an add-on incrementally.

### Documentation is both accessible and comprehensive

Camelot's application documentation is regularly used and referred to
today. This becomes even more important as Camelot expands into a
platform. The following forms of documentation will need to be provided:

1. **Getting started**: documentation to walk a new author through the process
   of building an add-on. This should be in the form of an walk-through of
   building a relatively simple example add-on.
2. **API Reference**: Reference documentation for add-ons and REST APIs should
   be well-written, comprehensive and accurate. It is also important that the
   documentation evolves with the API, and as such this would ideally be
   generated.
3. **Best practices**: Authors are writing software for others, to run on
   other systems. This is likely not something they do every day, and may find
   it useful to have a series of best practices available which they can use
   to improve the robustness of the software they write.

### Error messages are accurate and useful

No matter how well documented, or how simple, there will be errors encountered
by authors during add-on development and debugging will be necessary. Ensuring
authors are able to debug problems needs to be a priority. This should include:

* Where the payload (either REST, an add-on descriptor, file or other) is
  provided in a format which is invalid, the exact problem should be
  identified in the error message.
* Error messages should not be overly technical
* Where possible, reference documentation should be linked from the error
  message.

## Implementation strategy

A breakdown of the implementation strategy is as follows:

1. [provide administrative tooling for Camelot](#provide-administrative-tooling-for-camelot),
2. [introduce an add-on system](#introduce-an-add-on-system),
3. [incrementally offer extension points throughout Camelot](#Incrementally-offer-extension-points-throughout-camelot),
4. [introduce a stable, JSON REST API](#introduce-a-stable-json-rest-api),
5. [decompose Camelot, exercising new APIs with existing functionality](#decompose-camelot-exercising-new-apis-with-existing-functionality),
6. [provide excellent documentation and error messages](#provide-excellent-documentation-and-error-messages)

### Provide administrative tooling for Camelot

Administrative tooling will come in the form of the [Camelot
Launcher](https://gitlab.com/camelot-project/camelot-launcher). This will be a
desktop and CLI application which provides an interface for administrators to
configure Camelot.

The features it will provide are as follows:

* Start and stop the camelot server, and any other "server" add-ons.
* Allow for the data directory to be moved, including moving the database,
  media and filestore directories independently
* Allow add-ons to be installed, and the installed add-ons configured, viewed
  and removed
* Allow Camelot to be backed up (with and without media), and allow backups to
  be restored.
* Allow other built-in Camelot settings to be configured (server HTTP port,
  JVM heap size, etc.)
* Ability to revert settings to defaults

This will also allow for migrating away from environmment variables for
configuring Camelot, a frequent pain-point today.

### Introduce an add-on system

Camelot will allow authors flexibility in their technology stack by
integrating closely with standalone applications. Typically the integration
point will be via temporary files in a format defined by Camelot.

The exact format of data within the files will be defined on a case-by-case
basis, though [JSON](https://en.wikipedia.org/wiki/JSON) will be used as a
serialisation format for composite data. This is because:

1. **Ubiquity**: libraries for working with JSON are readily available in
   nearly every language, and it is very well known and widely used as a data
   exchange format,
2. **Human-readability**: JSON is human-readable, which aids understanding and
   debugging,
3. **Consistency**: JSON will also be used for the REST API, add-on
   descriptors and other integration points in Camelot.

This flexibility also means there may not be libraries available in a given
language to integrate with Camelot, leaving tasks such as serialisation to be
hand-rolled by the add-on authors. A later milestone may include providing
client libraries for Camelot for commonly-used languages.

Camelot will use a simple, JSON-based descriptor for add-ons, where the
functionality of an add-on is implemented as a stand-alone application.

```javascript
{
    "name": "My special report",
    "description": "A special report written by the community",
    "version": "0.0.1",
    "url": "https://gitlab.com/...",
    "license": "https://www.eclipse.org/legal/epl-v10.html",
    "requirements": [
        {
            "name": "R",
            "url": "https://www.r-project.org/"
        }
    ],
    "options": {
        "r_path": {
            "label": "Path to R executable",
            "default_value": "r",
            "datatype": "string"
        },
    },
    "features": [<feature descriptors...>]
```

A breakdown of the options for an add-on:

* `name`: Add-on name. Will be showed in the add-on listing and (in this
  case) in the reports UI.
* `description`: Description of the add-on. Will be shown in the Camelot
  Launcher add-on listing and (in this case) in the reports UI.
* `version`: Version of the add-on.
* `url`: URL to find more information about the add-on.
* `license`: License the add-on is published under.
* `requirements`: Any additional software which must be (manually) installed
  to use this script. Will be displayed in the Camelot Launcher UI.
* `options`: Options are available at an add-on level and will be available
  for configuration under the Camelot Launcher UI. Options are also available
  at a per-feature level.
* `features`: an array of *feature descriptors* provided by this add-on.

The above is the same for all add-ons. However the shape of a feature
descriptor will change depending on the `type` of feature. The below
demonstrate a feature descriptor possible features for a report.

An aside: sophisticated add-ons, such as the Camelot
[library](https://camelot-project.readthedocs.io/en/latest/library.html) will
be implemented as a HTTP server integrated with Camelot Server via REST
APIs. Their UI will also be provided as part of the add-on, though that would
necessarily be in JS or something that has been compiled down to it.

#### Report features

The below shows an example feature descriptor for a report:

```javascript
{
    "type": "report",
    "command": "{r_path} {script} {cli_arguments} {trap_station_id} {raw_data} {output_file}"
    "constants": {
        "script": "mySpecialReport.r",
    },
    "options": {
        "cli_arguments": {
            "label": "Command-line arguments",
            "default_value": "",
            "datatype": "string"
        }
    },
    "report_options": {
        "trap_station_id": {
            "cli_option": "--id",
            "label": "trap station name",
            "schema": {
                "type": "select",
                "required": true
                "get-options": {
                    "url": "/trap-stations",
                    "label": "trap-station-name"
                    "value" "trap-station-id"
                }
            },
        }
    },
    "data_source": "independent_sightings"
}
```

In the above example, the actual functionality would be provided by the R
script `mySpecialReport.r`, which would read raw report data from a given
filename (`in_file`), transform this into the desired report output (CSV), and
write that to the output file specified (`out_file`).

Note that `command` goes through an interpolation process.  This includes
built-in keys (`in_file`, `out_file`), and the values of named keys in
`constants`, `options` and `report_options`.

`options` are made available for configuration via the Camelot Launcher
UI. This is in constrast to `constants`, which are available for interpolation
but are not user-configurable.

To allow the user to limit the report, for example to a specific survey,
`report_options` can be used. Camelot will populate select options (e.g., show
a list of trapstations) but will not interpret these for the purpose of
reporting; it will be up to the add-on to interpret these values.

In the above the `cli_option` is an optional key which defines the
corresponding command-line option to set. The purpose of this is to facilitate
optional options -- the command-line option cannot be statically declared in
the command line if the option may or may not be provided. Instead we declare
the corresponding option to use here, and allow Camelot to supply it if
required.

The features descriptor with the `cli_option` paramater, and with commands
generally, make the assumption that these strings can be defined *statically*.
This may not necessarily be the case due to, for example, cross-platform
malarkey. Should this assumption prove false, add-on authors have the option
of supplying platform-specific descriptors or wrapping their script and
handling the cross-platform considerations themselves. It is expected be be
very rare, and therefore not worth introducing complications to account for it
in the add-on descriptor API.

For reports, `in_file` may be a file containing:

**TODO** Define `data` format.

### Incrementally offer extension points throughout Camelot

Extension points correspond to feature `type`s, as described above. Camelot
will provide for a number of feature types, and the available feature `type`s
will be extended over time.

The first candidate for extension will be reporting, followed by support for
additional filetypes being uploaded.

Later the library will be provided independently of Camelot.

Each extension point will likely be delivered separately, due to the amount of
work needed to extract each and to keep the surface area small should it be
necessary to revise APIs.

### Introduce a stable JSON REST API

Camelot will also provide a stable, well-defined JSON REST API to the Camelot
Server, which will gradually replace the changable internal
[Transit](https://github.com/cognitect/transit-clj) APIs currently in use
within Camelot.

This will more-or-less be a port of the existing Transit API.

### Decompose Camelot, exercising new APIs with existing functionality

In order to ensure the APIs provided are fit-for-purpose, explore the provided
functionality, and validate that everything is working as expected, some of
Camelot's existing functionality will be exposed via the add-on system.

All of Camelot's built-in reports will be exposed via the reporting
add-on. This will mean extracting the reporting DSL part of Camelot as a
library.

Similarly, Camelot's support for EXIF data extraction will be provided via an
add-on. This will open up the ability to import via video files as well and
support for transcoding video into web-friendly formats will also be provided
via add-ons.

Video add-ons will likely not be out-of-the-box, as the metadata standards for
videos (or lack thereof) introduce an element of questionable data
integrity. Camelot has in the past decided that video support cannot be
introduced in a way suitable to be "core".  However the ability to provide
functionality via add-ons:

1. allows better integration with best-of-breed tooling for managing video
   data (i.e., tooling that operates outside of the JVM), and
2. allows desirable functionality to be provided without compromising on
   design principles of Camelot-proper

Finally, and most excitingly, the library will be provided as an add-on. This
will demonstrate that Camelot is *highly* extensible. At this point add-ons
will be able to define:

1. new navigation items
2. their own (potentially sophisticated) frontend components
3. integrate the frontend with both the Camelot Server and an add-on-defined
   server process

The decomposition process also works to reduce the surface area of
Camelot. Aside from exercising APIs, this has a few benefits:

* makes it easier for someone to become confident in developing part of
  Camelot through isolation,
* may allow for pieces of Camelot to be superseded, depending on other
  advancements in the field.

### Provide excellent documentation and error messages

The add-on descriptor will be validated against a schema when imported,
providing useful error messages, which should help to debug mistakes in the
descriptor itself.

A REST payload should also be validated against a schema. Should the payload
not match the expected schema, it should be reported in the response the exact
cause of the mismatch.

**TODO**: Explore how swagger and spec can be leveraged to provide stable,
well-documented APIs.

**TODO**: Explore how spec can be used to provide excellent schema validation
and error reporting.

## Limitations

### Custom report deprecation

This will see the existing custom reports DSL removed after a deprecation
period in favour of all reports using the new add-ons API.

Custom reports set out with the goal of being able to define a report without
writing any code. The reporting DSL was a start towards this goal, but in
practice it was found that reporting relies heavily on executable code, which
is not conducive to a nice UI. This moves Camelot away from that vision
entirely.

### Report and import performance

Performance of small reports may be impacted due to poor Clojure startup
times.

Metadata extraction from imports may need to be rewritten into another
language, as a slow startup time (i.e., ~100+ ms) to extract image metadata is
unacceptable when considering that 10-100k images may be uploaded at a time
via bulk import.  If startup time cannot be made acceptable, it will need to
be ported to another language.

## Future

### R client library

With a solid REST API, Camelot can be taken further to have close integration
with R & CamtrapR. This may include defining a Camelot R client library over
the Camelot REST API and add-on APIs in order to ease adoption.

### Custom fields

Camelot has [sighting
fields](https://camelot-project.readthedocs.io/en/latest/sightingfields.html)
which allow arbitrary data to be captured along with sightings.

It would be beneficial to lift this up a level, and introduce custom fields
with the ability to provide this for any entity type. Under the above
proposal, Camelot would and should retain all data about its entities. Custom
fields should allow add-ons to include additional information about entities.

## License

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
