#!/bin/sh

set -e

PROJECT_NAME="camelot"
ELECTRON_VERSION="v4.0.6"

SCRIPT_DIR=`readlink -f $(dirname "${0}")`
PROJECT_DIR="${SCRIPT_DIR}/.."
BUILD_DIR="${PROJECT_DIR}/build"
DIST_DIR="${PROJECT_DIR}/dist"

CAMELOT_SERVER_REPO="https://gitlab.com/camelot-project/camelot"
CAMELOT_LAUNCHER_REPO="https://gitlab.com/camelot-project/camelot-launcher"

echo -n "camelot-server git ref (HEAD): "
read CAMELOT_SERVER_REF

if [ -z CAMELOT_SERVER_REF ]; then
    CAMELOT_SERVER_REF=HEAD
fi

echo -n "camelot-launcher git ref (HEAD): "
read CAMELOT_LAUNCHER_REF

if [ -z CAMELOT_SERVER_REF ]; then
    CAMELOT_LAUNCHER_REF=HEAD
fi

echo -n "Release version: "
read RELEASE_VERSION

PLATFORMS=( win32 darwin linux )
ARCHS=( x64 )

rm -rf ${BUILD_DIR}
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}

echo "Building camelot-launcher..."
git clone ${CAMELOT_LAUNCHER_REPO} "repo-temp"
cd "repo-temp"
git checkout ${CAMELOT_LAUNCHER_REF}
./script/build

echo "Patching main.js..."
sed -i 's/path.resolve(".")/__dirname/g' dist/main.js

mv dist "${DIST_DIR}/camelot-launcher"
cd ../
rm -rf "repo-temp"

echo "Building camelot..."
git clone ${CAMELOT_SERVER_REPO} "repo-temp"
cd "repo-temp"
git checkout ${CAMELOT_SERVER_REF}
./script/build
mv "target/${PROJECT_NAME}.jar" "${DIST_DIR}/${PROJECT_NAME}-${RELEASE_VERSION}.jar"
cd "../"
rm -rf "repo-temp"

cd ../

mkdir -p cache

build () {
    platform=$1
    arch=$2

    echo "Packaging for ${platform}-${arch}..."

    if [ ! -e "cache/electron-${ELECTRON_VERSION}-${platform}-${arch}.zip" ]; then
        cd cache
        curl -LO https://github.com/electron/electron/releases/download/${ELECTRON_VERSION}/electron-${ELECTRON_VERSION}-${platform}-${arch}.zip
        cd ../
    fi

    rm -rf "${BUILD_DIR}"
    mkdir -p "${BUILD_DIR}"
    cd "${BUILD_DIR}"

    mkdir -p "${PROJECT_NAME}-${RELEASE_VERSION}"

    cd "${PROJECT_NAME}-${RELEASE_VERSION}"
    unzip "../../cache/electron-${ELECTRON_VERSION}-${platform}-${arch}.zip"

    build_dir=$(pwd)

    if [ ${platform} == "darwin" ]; then
        app_dir="${build_dir}/Camelot.app/Contents/Resources/app"
        command_dir="${app_dir}"
        mv "Electron.app" "Camelot.app"
        cp "${PROJECT_DIR}/resources/logo.icns" "Camelot.app/Contents/Resources/electron.icns"
    else
        app_dir="${build_dir}/resources/app"
        command_dir="${build_dir}"
    fi

    mkdir -p ${app_dir}
    cd ${app_dir}

    cp -r "${DIST_DIR}/camelot-launcher"/* .

    cd ${build_dir}

    cp "${DIST_DIR}/${PROJECT_NAME}-${RELEASE_VERSION}.jar" "${command_dir}"

    if [ -e electron ]; then
        mv electron camelot
    elif [ -e electron.exe ]; then
        mv electron.exe Camelot.exe
    fi

    cd ../

    zip -r "${PROJECT_NAME}-${RELEASE_VERSION}-${platform}-${arch}.zip" "${PROJECT_NAME}-${RELEASE_VERSION}"
    mv "${PROJECT_NAME}-${RELEASE_VERSION}-${platform}-${arch}.zip" "${DIST_DIR}"

    echo "${PROJECT_NAME}-${RELEASE_VERSION}-${platform}-${arch}.zip... done"

    cd ../
}

if [ -z $1 ]; then
    for platform in "${PLATFORMS[@]}"; do
        for arch in "${ARCHS[@]}"; do
            build $platform $arch
        done
    done
else
    for arch in "${ARCHS[@]}"; do
        build $1 $arch
    done
fi

rm -rf "${DIST_DIR}/camelot-launcher"
rm -rf "${BUILD_DIR}"

echo "Uploading release... "
cd "${DIST_DIR}"

if [ -z $1 ]; then
    for platform in "${PLATFORMS[@]}"; do
        for arch in "${ARCHS[@]}"; do
            echo "Uploading ${PROJECT_NAME}-${RELEASE_VERSION}-${platform}-${arch}.zip..."
            aws s3 cp "${PROJECT_NAME}-${RELEASE_VERSION}-${platform}-${arch}.zip" s3://camelot-project/release/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
        done
    done
else
    for arch in "${ARCHS[@]}"; do
        echo "Uploading ${PROJECT_NAME}-${RELEASE_VERSION}-${1}-${arch}.zip..."
        aws s3 cp "${PROJECT_NAME}-${RELEASE_VERSION}-${1}-${arch}.zip" s3://camelot-project/release/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
    done
fi
